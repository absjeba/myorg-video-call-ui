import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MyService {

  loggedUser:String='';

  URL = 'https://localhost:3000'

  constructor(private http:HttpClient) { }


  signup(data){
      return this.http.post(this.URL+'/signup',data)
  }

  signin(data){
    return this.http.post(this.URL+'/api-login/login',data)
  }

  signout(){
    return this.http.post(this.URL+'/api-login/logout','')
  }

  getUsersList(){
    return this.http.get(this.URL+'/api-users/getUsers')
  }

  call(userDetail){
    return this.http.post(this.URL+'/api-call/'+userDetail.id,userDetail)
  }

  checkIncomingCall(){
    return this.http.get(this.URL+'/api-call/check')
  }

  setLoggedUser(user){
      this.loggedUser = user
  }

  

  getLoggedUser(){
    return this.loggedUser
  }
}
