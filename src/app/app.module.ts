import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatDialogModule, MatSnackBarModule } from "@angular/material";
import { FormsModule } from "@angular/forms";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { SigninComponent } from './signin/signin.component';
import { VidpageComponent } from './vidpage/vidpage.component';
import { HttpClientModule } from "@angular/common/http";
import { UserVideoComponent } from './user-video.component';
import { OpenViduVideoComponent } from './ov-video.component';
import { OpenviduSessionModule } from "openvidu-angular";
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    SigninComponent,
    VidpageComponent,
    UserVideoComponent,
    OpenViduVideoComponent,
    ConfirmationDialogComponent
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    OpenviduSessionModule,
    MatDialogModule,
    MatSnackBarModule
   
  ],
  providers: [],

  bootstrap: [AppComponent]
})
export class AppModule { }
