import { Component, OnInit, HostListener, OnDestroy, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OpenVidu, Publisher, Session,  StreamEvent, StreamManager, Subscriber, SessionDisconnectedEvent } from 'openvidu-browser';
import { throwError as observableThrowError, Observable, interval, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MyService } from '../my.service';
import { VERSION, MatDialogRef, MatDialog, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { UserModel, OpenviduSessionComponent, OpenViduLayoutOptions, OpenViduLayout, OvSettings } from "openvidu-angular";
import { eventNames, disconnect } from 'cluster';
import { ConfirmationDialogComponent } from "../confirmation-dialog/confirmation-dialog.component";

@Component({
  selector: 'app-vidpage',
  templateUrl: './vidpage.component.html',
  styleUrls: ['./vidpage.component.css']
})
export class VidpageComponent implements OnInit {

  parti:String='';
  sess:String='';
  userName:String='';

  usersList:any = []
  

  OPENVIDU_SERVER_URL = 'https://' + location.hostname + ':4443';
  OPENVIDU_SERVER_SECRET = 'MY_SECRET';

  // OpenVidu objects
  OV: OpenVidu;
  session1: Session;
  publisher: StreamManager; // Local
  subscribers: StreamManager[] = []; // Remotes

  // Join form
  mySessionId: string;
  myUserName: string;

  token:String;

  

  
  // Main video of the page, will be 'publisher' or one of the 'subscribers',
  // updated by click event in UserVideoComponent children
  mainStreamManager: StreamManager;

  tokens: string[] = [];
  session = false;

  ovSession: Session;
  ovLocalUsers: UserModel[];
  ovLayout: OpenViduLayout;
  ovLayoutOptions: OpenViduLayoutOptions;
  ovSettings: OvSettings;

  showDialog:Boolean;
  count = 0;

  @ViewChild('ovSessionComponent')
  public ovSessionComponent: OpenviduSessionComponent;

  
  constructor(private httpClient: HttpClient,private service:MyService,
        private router:Router, private dialog: MatDialog,
        private snackBar: MatSnackBar) {
    this.generateParticipantInfo();
    
    this.ovSettings = {
      chat: true,
      autopublish: true,
      toolbarButtons: {
        audio: true,
        video: true,
        screenShare: true,
        fullscreen: false,
        exit: true,
      }
    };

    this.requestEvery3Sec()
    
  }

  @HostListener('window:beforeunload')
  beforeunloadHandler() {
    // On window closed leave session
    this.leaveSession();
  }

  ngOnDestroy() {
    // On component destroyed leave session
    this.leaveSession();
  }

  ngOnInit(): void {
    this.userName  = this.service.getLoggedUser();
    this.getUsers();
   
  }


  getUsers(){
    this.service.getUsersList().toPromise()
      .then((res=>{
          this.usersList = res;
      }))

      // this.requestEvery3Sec();
  }

  openDialog() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent,{
      data:{
        message: 'Are you sure want to delete?',
        buttonText: {
          ok: 'Accept',
          cancel: 'Reject'
        }
      }
    });
    const snack = this.snackBar.open('Snack bar open before dialog');

    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        snack.dismiss();
        const a = document.createElement('a');
        a.click();
        a.remove();
        snack.dismiss();
        this.snackBar.open('Closing snack bar in a few seconds', 'Fechar', {
          duration: 2000,
        });
      }
    });
  }

  

  requestEvery3Sec(){

     var interval = setInterval(()=>{
 
        this.service.checkIncomingCall().toPromise()
            .then((res)=>{
              console.log(res)
     
              this.count++;
              if(res["dialog"]==="open" && this.count === 1 && res["from"]!=this.service.getLoggedUser()){
                var result = confirm("Message:"+res['msg'])
                if(result){
                  clearInterval(interval)                  
                    this.joinSessionCall(res["session"])               
                  
                }else{
                  clearInterval(interval);
                  alert("disconnected")
                }
              }else if(res["dialog"]==="close"){
                clearInterval(interval)
              }

              // clearInterval(interval);
              // this.openDialog();
            }).catch(err=>console.log("error:"+err))
      },5000);
  }

  disconnect(interval){
    clearInterval(interval)
  }

  enableCall(user){
     
    this.joinSession();
    let userDetail = {userName:user.user,id:user._id,session:this.mySessionId}
    alert(userDetail.userName);
    this.service.call(userDetail).toPromise()
      .then(res=>{
        // alert(res['msg']);
        // if(res["msg"]==="incoming call" && res["to"] === localStorage.getItem("loggedUser")){
        //   alert(res['to']);
          

        //   // confirm(`Incoming Call from ${res["from"]}`);

        // }
      })
      // this.requestEvery3Sec();
    
  }

  
  /* LOGOUT FUNCTION */
  logOut(){
      this.service.signout().toPromise()
        .then((res)=>{
          if(res["status"]==1){
            localStorage.getItem("loggedUser");
              this.router.navigate(['/signin']);
          }
        }).catch(err => console.log(err))
  }

  // /* JOIN SESSION */
  // joinSession() {

  //   // --- 1) Get an OpenVidu object ---

  //   this.OV = new OpenVidu();

  //   // --- 2) Init a session ---

  //   this.session1 = this.OV.initSession();
      
  //   // --- 3) Specify the actions when events take place in the session ---

  //   // On every new Stream received...
  //   this.session1.on('streamCreated', (event: StreamEvent) => {

  //     // Subscribe to the Stream to receive it. Second parameter is undefined
  //     // so OpenVidu doesn't create an HTML video by its own
  //     let subscriber: Subscriber = this.session1.subscribe(event.stream, undefined);
  //     this.subscribers.push(subscriber);
  //   });

    

  //   // On every Stream destroyed...
  //   this.session1.on('streamDestroyed', (event: StreamEvent) => {

  //     // Remove the stream from 'subscribers' array
  //     // this.deleteSubscriber(event.stream.streamManager);
  //   });

  //   // --- 4) Connect to the session with a valid user token ---

  //   // 'getToken' method is simulating what your server-side should do.
  //   // 'token' parameter should be retrieved and returned by your own backend
  //   this.getToken().then(token => {
  //     console.log(token)

  //     // First param is the token got from OpenVidu Server. Second param can be retrieved by every user on event
  //     // 'streamCreated' (property Stream.connection.data), and will be appended to DOM as the user's nickname
  //     this.session1.connect(token, { clientData: this.myUserName })
  //       .then(() => {
  //         this.session1.onNewMessage('incomingCall');
  //         this.session1.emitEvent('incomingCall',[StreamEvent])
  //         // --- 5) Get your own camera stream ---

  //         // Init a publisher passing undefined as targetElement (we don't want OpenVidu to insert a video
  //         // element: we will manage it on our own) and with the desired properties
  //         let publisher: Publisher = this.OV.initPublisher(undefined, {
  //           audioSource: undefined, // The source of audio. If undefined default microphone
  //           videoSource: undefined, // The source of video. If undefined default webcam
  //           publishAudio: true,     // Whether you want to start publishing with your audio unmuted or not
  //           publishVideo: true,     // Whether you want to start publishing with your video enabled or not
  //           resolution: '640x480',  // The resolution of your video
  //           frameRate: 30,          // The frame rate of your video
  //           insertMode: 'APPEND',   // How the video is inserted in the target element 'video-container'
  //           mirror: false           // Whether to mirror your local video or not
  //         });

  //         // --- 6) Publish your stream ---

  //         this.session1.publish(publisher);

  //         // Set the main video in the page to display our webcam and store our Publisher
  //         this.mainStreamManager = publisher;
  //         this.publisher = publisher;
          
  //       })
  //       .catch(error => {
  //         console.log('There was an error connecting to the session:', error.code, error.message,error);
  //       });

        
  //   });
  // }

  leaveSession() {

    // --- 7) Leave the session by calling 'disconnect' method over the Session object ---
    console.log("session Leave");
    // this.router.navigate(["/vidcall"])
    if (this.session1) { this.session1.disconnect(); console.log("Disconnect") };

    // Empty all properties...
    this.subscribers = [];
    delete this.publisher;
    delete this.session;
    delete this.OV;
    // this.generateParticipantInfo();
  }



  private generateParticipantInfo() {
    // Random user nickname and sessionId
    this.mySessionId = 'Session ' + Math.floor(Math.random() * 10);
    // this.myUserName = 'Participant' + Math.floor(Math.random() * 100);
    this.myUserName = this.service.getLoggedUser().toString()
  }

  // private deleteSubscriber(streamManager: StreamManager): void {
  //   let index = this.subscribers.indexOf(streamManager, 0);
  //   if (index > -1) {
  //     this.subscribers.splice(index, 1);
  //   }
  // }

  // updateMainStreamManager(streamManager: StreamManager) {
  //   this.mainStreamManager = streamManager;
  // }

  

  async joinSession() {
    
    const token1 = await this.getToken();
    const token2 = await this.getToken();
    this.tokens.push(token1, token2);
    this.session = true;
  }



  async joinSessionCall(sessionID) {
    alert(sessionID)
    const token1 = await this.getTokenCall(sessionID);
    const token2 = await this.getTokenCall(sessionID);
    this.tokens.push(token1, token2);
    this.session = true;
  }



  handlerSessionCreatedEvent(session: Session): void {

    // You can see the session documentation here
    // https://docs.openvidu.io/en/stable/api/openvidu-browser/classes/session.html

    console.log('SESSION CREATED EVENT', session);

    session.on('streamCreated', (event: StreamEvent) => {
      // Do something
      console.log("Stream Created");
    });

    session.on('streamDestroyed', (event: StreamEvent) => {
      // Do something
      console.log("Stream Destroyed");
    });

    session.on('sessionDisconnected', (event: SessionDisconnectedEvent) => {
      this.session = false;
      this.tokens = [];
      this.leaveSession()
      
    });

    this.myMethod();

  }

  handlerPublisherCreatedEvent(publisher: Publisher) {

    // You can see the publisher documentation here
    // https://docs.openvidu.io/en/stable/api/openvidu-browser/classes/publisher.html

    console.log('PUBLISHER CREATED EVENT', publisher);


    publisher.on('streamCreated', (e) => {
      console.log('Publisher streamCreated', e);
    });

    publisher.on('incomingCall',(e)=>{
      confirm("JOIN Call");
    })

  }

  handlerErrorEvent(event): void {
    // Do something
  }

  handlerLeaveSessionEvent(event): void {
    this.session = false;
    this.tokens = [];

    console.log("leavesession")
    this.leaveSession();
  }

  myMethod() {
    this.ovSession = this.ovSessionComponent.getSession();
    this.ovLocalUsers = this.ovSessionComponent.getLocalUsers();
    this.ovLayout = this.ovSessionComponent.getOpenviduLayout();
    this.ovLayoutOptions = this.ovSessionComponent.getOpenviduLayoutOptions();
  }
  



  /**
   * --------------------------
   * SERVER-SIDE RESPONSIBILITY
   * --------------------------
   * This method retrieve the mandatory user token from OpenVidu Server,
   * in this case making use Angular http API.
   * This behavior MUST BE IN YOUR SERVER-SIDE IN PRODUCTION. In this case:
   *   1) Initialize a session in OpenVidu Server	 (POST /api/sessions)
   *   2) Generate a token in OpenVidu Server		   (POST /api/tokens)
   *   3) The token must be consumed in Session.connect() method of OpenVidu Browser
   */

  getToken():Promise<string> {
    
    
    return this.createToken(this.mySessionId)

    
  }

  getTokenCall(sessionID):Promise<string> {
    
    
    return this.createTokenCall(sessionID)

    
  }

  

  createToken(sessionId): Promise<string> {
    return new Promise((resolve, reject) => {
      var user=this.service.getLoggedUser()
      console.log(user)
      const body = JSON.stringify({ session: sessionId,user:user });
      const options = {
        headers: new HttpHeaders({
          // 'Authorization': 'Basic ' + btoa('OPENVIDUAPP:' + this.OPENVIDU_SERVER_SECRET),
          'Content-Type': 'application/json'
        })
      };
      return this.httpClient.post(  'https://localhost:3000/api-sessions/get-token', {sessionName:this.mySessionId,user:user}, options)
        .pipe(
          catchError(error => {
            reject(error);
            return observableThrowError(error);
          })
        )
        .subscribe(response => {
          console.log(response);
          resolve(response['token']);
        });
    });
  }


  createTokenCall(sessionId): Promise<string> {
    return new Promise((resolve, reject) => {
      var user=this.service.getLoggedUser()
      console.log(user)
      const body = JSON.stringify({ session: sessionId,user:user });
      const options = {
        headers: new HttpHeaders({
          // 'Authorization': 'Basic ' + btoa('OPENVIDUAPP:' + this.OPENVIDU_SERVER_SECRET),
          'Content-Type': 'application/json'
        })
      };
      return this.httpClient.post(  'https://localhost:3000/api-sessions/get-token', {sessionName:sessionId,user:user}, options)
        .pipe(
          catchError(error => {
            reject(error);
            return observableThrowError(error);
          })
        )
        .subscribe(response => {
          console.log(response);
          resolve(response['token']);
        });
    });
  }

}


