import { Component, OnInit } from '@angular/core';
import { MyService } from '../my.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  email:String = '';
  password:String ='';

  constructor(private service:MyService,private router:Router) { }

  ngOnInit(): void {
  }

  login(frmdata){
    this.service.signin(frmdata).toPromise()
        .then((res)=>{
            console.log(res)
            this.service.setLoggedUser(res["user"])
            localStorage.setItem("loggedUser",res["user"])
          this.router.navigate(['/vidcall'])
        }).catch((err)=>{
            console.log(err)
        })
  }

}
