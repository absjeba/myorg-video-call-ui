import { Component, OnInit } from '@angular/core';
import { MyService } from '../my.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email:String = '';
  pass:String ='';
  rol:String ='';

  constructor(private service:MyService) { }

  ngOnInit(): void {
  }

  register(formdata){
      this.service.signup(formdata).toPromise()
        .then((res)=>{
            console.log(res)
        }).catch((err)=>  {
            console.log(err)
        })
  }

}
