import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { SigninComponent } from './signin/signin.component';
import { VidpageComponent } from './vidpage/vidpage.component';


const routes: Routes = [
  {path:'signup',component:RegisterComponent},
  {path:'signin', component:SigninComponent},
  {path:'vidcall',component:VidpageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
